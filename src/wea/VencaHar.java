package wea;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class VencaHar {
    public static void main(String[] args) throws IOException, InterruptedException {

        //BrowserMobProxy
        BrowserMobProxy server = new BrowserMobProxyServer();
        server.start(0);
        server.setHarCaptureTypes(CaptureType.getAllContentCaptureTypes());
        server.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);
        server.newHar("Venca");

        //Chrome options
        ChromeOptions cliArgsCap = new ChromeOptions();
        cliArgsCap.addArguments("headless");
        cliArgsCap.addArguments("proxy-server=localhost:"+server.getPort());

		System.setProperty("webdriver.chrome.driver","C:\\Users\\aleixabengochea\\eclipse-workspace\\Waterfall\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(cliArgsCap);
		
        //WebDriver
        driver.get("https://www.venca.es/");
        
        //Data
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    	Date date = new Date();

        //HAR
        Har har = server.getHar();
        FileOutputStream fos = new FileOutputStream("C:\\Users\\AleixAbengochea\\Desktop\\QA_Aleix\\har\\HAR_Venca_"+dateFormat.format(date)+".har");
        har.writeTo(fos);
        server.stop();
        driver.close();
        System.out.println("DONE");
    }
}